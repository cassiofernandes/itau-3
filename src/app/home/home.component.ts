import { Component, OnInit } from '@angular/core';
import { ServicesAplicationService } from '../services/services-aplication.service';
import { Subscription, forkJoin } from 'rxjs';
import { Geral } from '../models/geral';
import { Categoria } from '../models/categoria';
import { mergeMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  subGeral: Subscription;
  subCategoria: Subscription;

  janC: boolean = true;
  fevC: boolean = true;
  marC: boolean = true;
  abrC: boolean = true;
  maiC: boolean = true;
  junC: boolean = true;
  julC: boolean = true;
  agoC: boolean = true;
  setC: boolean = true;
  outC: boolean = true;
  novC: boolean = true;
  dezC: boolean = true;


  listGeral: Geral[];
  listCategoria: Categoria[];

  public nomeTransp: string;
  public nomeCompra: string;
  public nomeSaude: string;
  public nomeSerco: string;
  public nomeRest: string;
  public nomeMerc: string;

  public valorTransp: any;
  public valorCompra: any;
  public valorSaude: any;
  public valorSerco: any;
  public valorRest: any;
  public valorMerc: any;

  public valorJan: any; 
  public valorFev: any;
  public valorMar: any;
  public valorAbr: any;
  public valorMai: any;
  public valorJun: any;
  public valorJul: any;
  public valorAgo: any;
  public valorSet: any;
  public valorOut: any;
  public valorNov: any;
  public valorDez: any;
  
  
  
  
    

  constructor(
    private service: ServicesAplicationService
  ) { }

  ngOnInit() {
    this.listaGeral();
    this.lista();
  }

  listaGeral() {
    this.subGeral = this.service.listaGeralDados()
      .subscribe(res => {
        this.listGeral = res
        this.listaPorMes(res)     
      }
      , err => console.error(err))
  }
  listaPorMes(res) {

    res.sort(function(a,b) {
      return a.mes_lancamento < b.mes_lancamento ? -1 : a.mes_lancamento > b.mes_lancamento ? 1 : 0;
    });

    let listaObjJ = res.filter(resp => resp['mes_lancamento'] === 1);
        let listaObjF = res.filter(resp => resp['mes_lancamento'] === 2);
        let listaObjM = res.filter(resp => resp['mes_lancamento'] === 3);
        let listaObjA = res.filter(resp => resp['mes_lancamento'] === 4);
        let listaObjMa = res.filter(resp => resp['mes_lancamento'] === 5);
        let listaObjJU = res.filter(resp => resp['mes_lancamento'] === 6);
        let listaObjJL = res.filter(resp => resp['mes_lancamento'] === 7);
        let listaObjAg = res.filter(resp => resp['mes_lancamento'] === 8);
        let listaObjS = res.filter(resp => resp['mes_lancamento'] === 9);
        let listaObjO = res.filter(resp => resp['mes_lancamento'] === 10);
        let listaObjN = res.filter(resp => resp['mes_lancamento'] === 11);
        let listaObjD = res.filter(resp => resp['mes_lancamento'] === 12);

        if(!listaObjJ.length){
          this.janC = false;
        }  
        if(!listaObjF.length) {
          this.fevC = false;
        } 
        if(!listaObjM.length) {
          this.marC = false;
        }
        if(!listaObjA.length) {
          this.abrC = false;
        } 
        if(!listaObjMa.length) {
          this.maiC = false;
        }
        if(!listaObjJU.length) {
          this.junC = false;
        } 
        if(!listaObjJL.length) {
          this.julC = false;
        }
        if(!listaObjAg.length) {
          this.agoC = false;
        }  
        if(!listaObjS.length) {
          this.setC = false;
        }
        if(!listaObjO.length) {
          this.outC = false;
        }
        if(!listaObjN.length) {
          this.novC = false;
        }
        if(!listaObjD.length) {
          this.dezC = false;
        }

        let calcJan = listaObjJ.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcFev = listaObjF.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcMar = listaObjM.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcAbr = listaObjA.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcMa = listaObjMa.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcJu = listaObjJU.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcJl = listaObjJL.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcAg = listaObjAg.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);
        
        let calcS = listaObjS.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);
        
        let calcO = listaObjO.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcN = listaObjN.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

        let calcD = listaObjD.reduce((sum, mes) => {
          return sum + mes.valor;
        }, 0);

         this.valorJan = calcJan.toFixed(2);
         this.valorFev = calcFev.toFixed(2);
         this.valorMar = calcMar.toFixed(2);
         this.valorAbr = calcAbr.toFixed(2);
         this.valorMai = calcMa.toFixed(2);
         this.valorJun = calcJu.toFixed(2);
         this.valorJul = calcJl.toFixed(2);
         this.valorAgo = calcAg.toFixed(2);
         this.valorSet = calcS.toFixed(2);
         this.valorOut = calcO.toFixed(2);
         this.valorNov = calcN.toFixed(2);
         this.valorDez = calcD.toFixed(2);
       
  }


  lista(){
    let geral = this.service.listaGeralDados();
    let categoria = this.service.listaCategorias();

    forkJoin([geral, categoria]).subscribe(
      res => this.filtraCategoriasGeral(res[0], res[1])
      , err => console.error(err)
    )
  }

  filtraCategoriasGeral(geral, categoria){
    
    this.nomeTransp = categoria[0]['nome'];
    this.nomeCompra = categoria[1]['nome'];
    this.nomeSaude = categoria[2]['nome'];
    this.nomeSerco = categoria[3]['nome'];
    this.nomeRest = categoria[4]['nome'];
    this.nomeMerc = categoria[5]['nome'];

    let ft = geral.filter(res => res['categoria'] === 1);
    let fc = geral.filter(res => res['categoria'] === 2);
    let fs = geral.filter(res => res['categoria'] === 3);
    let fss = geral.filter(res => res['categoria'] === 4);
    let fr = geral.filter(res => res['categoria'] === 5);
    let fm = geral.filter(res => res['categoria'] === 6);

    let calcT = ft.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);
    let calcC = fc.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcS = fs.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);
    
    let calcSS = fss.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcR = fr.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcM = fm.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    this.valorTransp = calcT.toFixed(2);
    this.valorCompra = calcC.toFixed(2);
    this.valorSaude = calcS.toFixed(2); 
    this.valorSerco = calcSS.toFixed(2);
    this.valorRest = calcR.toFixed(2);
    this.valorMerc = calcM.toFixed(2);
  }


  listaCategorias() {
    this.subCategoria = this.service.listaCategorias()
      .subscribe(res => this.listCategoria = res);
  }

  ngOnDestroy(): void {
    this.subGeral.unsubscribe();
    
  }

}
