import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Geral } from '../models/geral';
import { Categoria } from '../models/categoria';

@Injectable({
  providedIn: 'root'
})
export class ServicesAplicationService {

  private readonly URL_GERAL = 'https://desafio-it-server.herokuapp.com/lancamentos';
  private readonly URL_CATEGORIA = 'https://desafio-it-server.herokuapp.com/categorias';

  constructor(
    private http: HttpClient
  ) { }

  listaGeralDados(){
    return this.http.get<Geral[]>(this.URL_GERAL);
  }

  listaCategorias(){
    return this.http.get<Categoria[]>(this.URL_CATEGORIA);
  }
}
