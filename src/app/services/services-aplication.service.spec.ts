import { TestBed } from '@angular/core/testing';

import { ServicesAplicationService } from './services-aplication.service';

describe('ServicesAplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicesAplicationService = TestBed.get(ServicesAplicationService);
    expect(service).toBeTruthy();
  });
});
